SECTION "Timer", ROM0[TimerInt]
Timer::
	di
	push bc
	push de
	push hl
	push af
	ld a, BANK(DevSound)	;ld a, [BankSave1]	; bank save 1
	ld [BankWrite], a
	call DS_Play			;call RunMusic		; run music
	call RunSFX		; run sfx
	ld a, [BankSave2]	; bank save 2
	ld [BankWrite], a
	; run timer
	pop af
	pop hl
	pop de
	pop bc
	ei
	reti