SECTION "LoadMusic", ROM0[MusLoad]
LoadMusic:
	jp _LoadMusic

SECTION "LoadMusicJump", ROM0[_MusLoad]
_LoadMusic:
	ld a, [BankSave2]
	push af
	ld a, BANK(DevSound)
	ld [BankWrite], a
	ld a, [currentMusId]
	dec a
	call DS_Init
	pop af
	ld [BankSave2],a
	ld [BankWrite], a
	ret