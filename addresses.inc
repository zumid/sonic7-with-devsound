; Original hooks

TimerInt	equ	$1ac
InitSong	equ $113c
RunMusic	equ $11ee
RunSFX		equ $1658
BankWrite	equ	$2333
BankSave1	equ $e21c
BankSave2	equ $c299
currentMusId equ $c606
MusLoad		equ	$3842
_MusLoad	equ	$3f2e

; New hooks

DevSound_Bank   equ $20
DevSound_Offset equ $4000
DevSound_RAM    equ $c700
