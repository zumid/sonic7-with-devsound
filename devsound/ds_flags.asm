; DevSound flags

; Uncomment the following line to enable custom hooks.
; UseCustomHooks		set	1

; Uncomment the following line if you want to include song data elsewhere.
; Could be useful for multibank setups.
; TODO: Make some tweaks to song data format to allow for multiple banks
; DontIncludeSongData	set	1

; Comment the following line to disable SFX support (via FX Hammer).
; Useful if you want to use your own sound effect system.
; (Note that DevSound may require some minor modifications if you
; want to use your own SFX system.)
UseFXHammer				set	1

; Uncomment this to disable all time-consuming features
; This includes: wave buffer, PWM, random wave, zombie mode,
; wave volume scaling, channel volume
; WARNING: Any songs that use the additional features will crash DevSound!
; DemoSceneMode 		set	1

; Uncomment this to to disable wave volume scaling.
; PROS: Less CPU usage
; CONS: Less volume control for CH3
; NoWaveVolumeScaling	set	1

; Uncomment this to disable zombie mode (for compatibility
; with old emulators such as VBA).
; NOTE: Zombie mode is known to be problematic with certain
; GBC CPU revisions. If you want your game/demo to be
; compatible with all GBC hardware revisions, I would
; recommend disabling this.
; PROS: Less CPU usage
;		Compatible with old emulators such as VBA
; CONS: Volume envelopes will sound "dirtier"
;DisableZombieMode		set	1

; Comment this line to enable Deflemask compatibility hacks.
DisableDeflehacks		set	1

; Uncomment this line for a simplified echo buffer. Useful if RAM usage
; is a concern.
; PROS: Less RAM usage
; CONS: Echo delay will be disabled
; SimpleEchoBuffer		set	1
