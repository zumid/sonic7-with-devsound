; =================================================================
; Pulse/Wave sequences
; =================================================================

WaveTable:
	dw	wave_Bass
	dw	DefaultWave
	dw	wave_PWMB
	dw	wave_PseudoSquare
	dw	wave_GSCWave3
	dw  wave_NewWave

wave_Bass:			db	$00,$01,$11,$11,$22,$11,$00,$02,$57,$76,$7a,$cc,$ee,$fc,$b1,$23
wave_PWMB:			db	$ff,$ff,$ff,$ff,$ff,$f0,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
wave_PseudoSquare:	db	$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$00,$00,$00,$44,$44,$00,$00,$00
wave_GSCWave3:		db	$02,$46,$8a,$cd,$ef,$fe,$de,$ff,$ee,$dc,$ba,$98,$76,$54,$32,$10
wave_NewWave:       db  $08,$99,$88,$99,$a9,$aa,$aa,$bb,$bc,$9d,$b7,$96,$72,$54,$22,$10

; use $c0 to use the wave buffer
waveseq_Bass_:		db	0,$ff
waveseq_Tri:		db	1,$ff
waveseq_PulseLead:	db	2,$ff
waveseq_Square_:	db	$c0,$ff
waveseq_GSCWave3:	db	4,$ff
waveseq_NewWave:	db	5,$ff

waveseq_Arp:		db	2,2,2,1,1,1,0,0,0,3,3,3,$fe,0
waveseq_OctArp:		db	2,2,2,1,1,2,$ff

waveseq_Bass:		db	1,1,1,1,1,1,2,2,2,2,2,2,3,3,3,3,3,3,0,0,0,0,0,0,$fe,0
waveseq_Square:		db	2,$ff
waveseq_Arp2:		db	0,0,0,0,1,1,1,2,2,2,2,3,3,3,2,2,2,2,1,1,1,$fe,00

waveseq_EchoTest:	db	1,$ff
waveseq_CNZMainLead:db  0, 0, 1, 1, 2, $ff
