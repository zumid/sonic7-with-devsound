; =================================================================
; Instruments
; =================================================================

InstrumentTable:
	const_def
	dins	Gadunk
	dins	Arp1
	dins	Arp2
	dins	OctArp
	dins	Bass1
	dins	Bass2
	dins	Bass3
	dins	Bass4
	dins	GadunkWave

	dins	Kick
	dins	Snare
	dins	CHH
	dins	OHH
	dins	CymbQ
	dins	CymbL

	dins	PulseBass
	dins	Tom
	dins	Arp
	dins	WaveLeadShort
	dins	WaveLeadMed
	dins	WaveLeadMed2
	dins	WaveLeadLong
	dins	WaveLeadLong2

	dins	Echo1
	dins	Echo2

	dins	AKick
	dins	ASnare
	dins	ACHH
	dins	AOHH
	dins	ACymb

	dins	Tom2
	dins	PWM1
	dins	Arp017C
	dins	Arp057C
	dins	PulseBass2
	dins	Arp950
	dins	Arp740
	dins	Arp830

	dins	RDLPulse1
	dins	RDLPulse2
	dins	RDLWave
	dins	RDLNoise

	dins	EgyptBass1
	dins	EgyptBass2
	
	dins	MontyTest1
	dins	MontyTest2
	
	dins	CNZFillLead
	dins	CNZMainLead
	dins	CNZEchoFill
	dins	TomWave

; Instrument format: [no reset flag],[voltable id],[arptable id],[wavetable id],[vibtable id]
; _ for no table
; !!! REMEMBER TO ADD INSTRUMENTS TO THE INSTRUMENT POINTER TABLE !!!
ins_Gadunk:			Instrument	0,Gadunk,Gadunk,Bass_,_
ins_Arp1:			Instrument	0,Arp,Pluck059,Arp,_
ins_Arp2:			Instrument	0,Arp,Pluck047,Arp,_
ins_OctArp:			Instrument	0,OctArp,Octave,OctArp,_
ins_Bass1:			Instrument	0,Bass1,Pluck,Bass_,_
ins_Bass2:			Instrument	0,Bass2,Pluck,Bass_,_
ins_Bass3:			Instrument	0,Bass3,Pluck,Bass_,_
ins_Bass4:			Instrument	0,Bass4,Pluck,Bass_,_
ins_GadunkWave:		Instrument	0,Bass1,Gadunk,Tri,_
ins_Kick:			Instrument	0,Kick,Kick,_,_
ins_Snare:			Instrument	0,Snare,Snare,_,_
ins_CHH:			Instrument	0,Kick,Hat,_,_
ins_OHH:			Instrument	0,OHH,Hat,_,_
ins_CymbQ:			Instrument	0,CymbQ,Hat,_,_
ins_CymbL:			Instrument	0,CymbL,Hat,_,_

ins_PulseBass:		Instrument	0,PulseBass,Pluck,Bass,_
ins_Tom:			Instrument	0,Tom,Tom,Square,_
ins_TomWave:			Instrument	0,Bass1,Tom,Tri,_
ins_Arp:			Instrument	0,Arp2,Buffer,Arp2,_

ins_WaveLeadShort:	Instrument	0,WaveLeadShort,Pluck,PulseLead,_
ins_WaveLeadMed:	Instrument	0,WaveLeadMed,Pluck,PulseLead,_
ins_WaveLeadMed2:	Instrument	0,WaveLeadLong,Pluck,NewWave,Test2
ins_WaveLeadLong:	Instrument	0,WaveLeadLong,Pluck,PulseLead,_
ins_WaveLeadLong2:	Instrument	0,WaveLeadLong2,Pluck,PulseLead,_

ins_Echo1:			Instrument	0,Echo1,Pluck,EchoTest,Test
ins_Echo2:			Instrument	0,Echo2,Pluck,EchoTest,Test

ins_CNZFillLead:	Instrument	0,CNZFill,Pluck,Square,Test2
ins_CNZMainLead:	Instrument	0,CNZFill,Pluck,CNZMainLead,Test2
ins_CNZEchoFill:    Instrument  0,CNZEcho,Pluck,Square,Test2

ins_AKick:			Instrument	0,McAlbyKick,McAlbyKick,_,_
ins_ASnare:			Instrument	0,McAlbySnare,McAlbySnare,_,_
ins_ACHH:			Instrument	0,McAlbyCHH,McAlbyHat,_,_
ins_AOHH:			Instrument	0,McAlbyOHH,McAlbyHat,_,_
ins_ACymb:			Instrument	0,McAlbyCymb,McAlbyCymb,_,_

ins_Tom2:			Instrument	0,Tom2,Tom,Square,_
ins_PWM1:			Instrument	0,WaveLeadShort,Pluck,Square_,_
ins_Arp017C:		Instrument	0,PulseBass2,_017C,OctArp,_
ins_Arp057C:		Instrument	0,PulseBass2,_057C,OctArp,_
ins_PulseBass2:		Instrument	0,PulseBass2,Pluck,Bass,_
ins_Arp950:			Instrument	0,PulseBass2,_950,OctArp,_
ins_Arp740:			Instrument	0,PulseBass2,_740,OctArp,_
ins_Arp830:			Instrument	0,PulseBass2,_830,OctArp,_

ins_RDLPulse1:		Instrument	0,Echo1,_,Bass_,_
ins_RDLPulse2:		Instrument	0,c7,_,Bass_,_
ins_RDLWave:		Instrument	0,Bass1,_,GSCWave3,_
ins_RDLNoise:		Instrument	0,Echo1,RDLNoise,_,_

ins_EgyptBass1:		Instrument	0,EgyptBass,Pluck,PulseLead,_
ins_EgyptBass2:		Instrument	0,EgyptBass,Pluck,Bass_,_

ins_MontyTest1:		Instrument	0,Echo1,_,EchoTest,Test2
ins_MontyTest2:		Instrument	0,Echo1,_740,EchoTest,Test2
