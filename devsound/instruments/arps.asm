; =================================================================
; Arpeggio/Noise sequences
; =================================================================

s7	equ	$2d

; Noise values are the same as Deflemask, but with one exception:
; To convert 7-step noise values (noise mode 1 in deflemask) to a
; format usable by DevSound, take the corresponding value in the
; arpeggio macro and add s7.
; Example: db s7+128+32 = noise value 32 with step lengh 7
; Note that each noiseseq must be terminated with a loop command
; ($fe) otherwise the noise value will reset!

arp_Gadunk: 	db	20,22,19,14,20,5,0,15,20,$ff
arp_Pluck059:	db	19,0,5,5,9,9,0,$fe,1
arp_Pluck047:	db	19,0,4,4,7,7,0,$fe,1
arp_Octave:		db	0,19,12,12,0,0,0,0,12,$fe,2
arp_Pluck:		db	12,0,$ff
arp_Tom:		db	22,20,18,16,14,12,10,9,7,6,4,3,2,1,0,$ff

arp__017C:		db	12,12,7,7,1,1,0,0,$fe,0
arp__057C:		db	12,12,7,7,5,5,0,0,$fe,0
arp__950:		db	9,5,0,9,9,5,5,0,0,$fe,3
arp__740:		db	7,4,0,7,7,4,4,0,0,$fe,3
arp__830:		db	8,3,0,8,8,3,3,0,0,$fe,3

arp_Kick:	db	$a0,$9a,$a5,$fe,2
arp_Snare:	db	s7+$9d,s7+$97,s7+$94,$a3,$fe,3
arp_Hat:	db	$a9,$ab,$fe,1

arp_McAlbyKick:	db	$aa,$9c,$98,$94,$8c,$94,$9c,$fe,6
arp_McAlbySnare:	db	$9c,$98,$9c,$a0,$a4,$a8,$a8,$fe,6
arp_McAlbyHat:		db	$a8,$aa,$ac,$fe,2
arp_McAlbyCymb:	db	$a8,$aa,$a4,$fe,2

arp_RDLNoise:	db	$ac,$ac,$ac,$ac,$ac,$ab,$ab,$ab,$ab,$aa,$aa,$aa,$aa,$aa,$a8,$a8,$a8,$a8,$a4,$a4,$a4,$a4,$a4,$a0,$a0,$a0,$a0
				db	$98,$98,$98,$98,$98,$94,$94,$94,$94,$90,$90,$90,$90,$90,$8c,$8c,$8c,$8c,$88,$88,$88,$88,$88,$84,$84,$84,$84,$80,$fe,54
