; =================================================================
; Volume sequences
; =================================================================

; For pulse and noise instruments, volume control is software-based by default.
; However, when the table execution ends ($FF) the value after that terminator
; will be loaded as a hardware volume and envelope. Please be cautious that the
; envelope speed won't be scaled along the channel volume.

; For wave instruments, volume has the same range as the above (that's right,
; this is possible by scaling the wave data) except that it won't load the
; value after the terminator as a final volume.
; WARNING: since there's no way to rewrite the wave data without restarting
; the wave so make sure that the volume doesn't change too fast that it
; unintentionally produces sync effect.
; NOTE: If the DisableWaveScaling flag is enabled, the above does not apply.
; Instead, there are four volume values (including 0). These values can be
; selected with w0-w3.

w0	equ	0
w1	equ	3
w2	equ	7
w3	equ	15

vol_Gadunk: 		db	15,5,10,5,2,6,10,15,12,6,10,7,8,9,10,15,4,3,2,1,$fe,0
vol_Arp:			db	8,8,8,7,7,7,6,6,6,5,5,5,4,4,4,4,3,3,3,3,3,2,2,2,2,2,2,1,1,1,1,1,1,1,1,0,$ff,0
vol_OctArp:			db	12,11,10,9,9,8,8,8,7,7,6,6,7,7,6,6,5,5,5,5,5,5,4,4,4,4,4,4,4,3,3,3,3,3,3,2,2,2,2,2,2,1,1,1,1,1,1,0,$ff,0
vol_Bass1:			db	w3,$ff
vol_Bass2:			db	w3,w3,w3,w3,w1,$ff
vol_Bass3:			db	w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w2,w2,w2,w2,w1,$ff
vol_Bass4:			db	w3,w3,w3,w3,w3,w3,w3,w2,w2,w2,w2,w1,$ff
vol_PulseBass:		db	15,15,14,14,13,13,12,12,11,11,10,10,9,9,8,8,8,7,7,7,6,6,6,5,5,5,4,4,4,4,3,3,3,3,2,2,2,2,2,1,1,1,1,1,1,0,$ff,0
vol_PulseBass2:		db	15,14,13,12,11,11,10,10,9,9,8,8,7,7,7,6,6,6,5,5,5,5,4,4,4,4,3,3,3,3,3,2,2,2,2,2,2,1,1,1,1,1,1,1,1,0,$ff,0

vol_Tom:			db	$ff,$f1
vol_Tom2:			db	$ff,$f3
vol_WaveLeadShort:	db	w3,w3,w3,w3,w2,$ff
vol_WaveLeadMed:	db	w3,w3,w3,w3,w3,w3,w3,w2,$ff
vol_WaveLeadLong:	db	w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w2,$ff
vol_WaveLeadLong2:	db	w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w3,w2,w2,w2,w2,w2,w2,w2,w2,w2,w2,w2,w2,w2,w2,w1,$ff
vol_Arp2:			db	$ff,$f2

vol_Kick:			db	$ff,$81
vol_Snare:			db	$ff,$d1
vol_OHH:			db	$ff,$84
vol_CymbQ:			db	$ff,$a6
vol_CymbL:			db	$ff,$f3

vol_Echo1:			db	12,$fd,3,$fe,2
vol_Echo2:			db	4,$fd,1,$fe,2
vol_c7:				db	$ff,$c7

vol_McAlbyKick:		db	15,15,13,9,7,5,$ff,$41
vol_McAlbyCHH:		db	8,6,4,$ff,$23
vol_McAlbyOHH:		db	10,6,3,$ff,$23
vol_McAlbySnare:	db	15,15,15,10,3,4,$ff,$53
vol_McAlbyCymb:		db	12,8,6,$ff,$54

vol_EgyptBass:		db	15,15,15,15,15,15,15,15,$51,$fe,$8

vol_CNZFill:    db 13, 12, 10, 9, 7, $fe, 4
vol_CNZEcho:    db 11, 10, 9, 9, 7, 6, 6, 5, $fe, 7
