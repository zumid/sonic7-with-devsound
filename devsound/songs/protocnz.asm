; =================================================================
; Carnival Night Zone 2 (S&KC / Sonic 3 Proto)
; =================================================================

PT_CNZ2_Proto:
    dw PT_CNZ2_Proto_CH1
    dw PT_CNZ2_Proto_CH2
    dw PT_CNZ2_Proto_CH3
    dw PT_CNZ2_Proto_CH4

PT_CNZ2_Proto_CH1:
	;00
	db	SetInstrument,id_OctArp
	dbw CallSection, .call0a
	db	SetInstrument,id_CNZFillLead
	db  G_3, 2, B_3, 2, D_4, 2, G_4, 2, A#4, 2, rest, 2, D_5, 2
	
	;01
	db	SetInstrument,id_OctArp
	dbw CallSection, .call0b
	db	SetInstrument,id_CNZFillLead
	db  G_3, 2, B_3, 2, D_4, 2, E_4, 2, G_4, 2, B_4, 2, D_5, 2
	
	; 02, 03
	;db	SetLoopPoint
	db	SetInstrument,id_CNZMainLead
	
	dbw CallSection, .call1
	db  A#4, 2
	db  A#4, 2, rest, 2
	db  A#4, 2, ChannelVolume, 5, A#4, 2, ChannelVolume, 15
	db  A#4, 2, rest, 2
	db  A#4, 2
	db  A#4, 2
	db  PitchBendDown, 12, ___, 4
	db  PitchBendDown, 0, rest, 2, TonePorta, 18, G_4, 2, rest, 2
	db  TonePorta, 0, F_4, 2, ChannelVolume, 5, E_4, 2, ChannelVolume, 15
	
	dbw CallSection, .call1
	db  C_5, 2
	db  F_5, 2, rest, 2
	db  C_5, 2, rest, 2
	db  F_5, 2, rest, 2
	db  C_5, 2
	db  F_5, 2
	db  PitchBendDown, 12, ___, 4, PitchBendDown, 0
	db  rest, 2
	db  rest, 2, rest, 2, rest, 2, rest, 2 ;idk what to put here
	;db	GotoLoopPoint
	
	db	SetInstrument,id_CNZEchoFill
	
	dbw CallSection, .call0c
	db  C_6,4
	dbw CallSection, .call0d
	dbw CallSection, .call0c
	db  C_6,4
	
	db  G_5,2, F_5,2, G_5,2, G_5,2, ChannelVolume,5, ___,2, ChannelVolume,15
	db  A#5,2, A#5,2, A#5,2, ChannelVolume,5, ___,2, ChannelVolume,15
	db  C_6,2, C_6,2, C_6,2, ChannelVolume,5, ___,2, ChannelVolume,15
	db  D_6,2, D_6,4
	
	dbw CallSection, .call0c
	db  C_6,4
	dbw CallSection, .call0d
	dbw CallSection, .call0c
	db  F_6,4
	
	db  G_5,4, F_5,2, G_5,2, ChannelVolume,5, ___,2, ChannelVolume,15
	db  A#5,2, ChannelVolume,5, ___,2, ChannelVolume,15
	db  A#5,2, ChannelVolume,5, ___,2, ChannelVolume,15
	db  G_5,2, ChannelVolume,5, ___,2, ChannelVolume,15
	db  F_5,4,PitchBendDown, 12, ___, 6, PitchBendDown, 0
	
	;;;
	
	db	EndChannel
	
.call0c
	db  G#4,2, D#5,2, G#5,2, D#5,2, G#4,2, D#5,2, G#5,2, D#6,4, D#6,2, D_6,2
	db  ChannelVolume, 5, D#5, 2, ChannelVolume, 15, D#6,4
	ret

.call0d
	db  D#4,2, A#4,2, D#5,2, A#4,2, D#4,2, A#4,2, D#5,2, A#5,4, A#5,2, C_6,2
	db  ChannelVolume, 5, A#5, 2, ChannelVolume, 15, D#5,4, G#5,4
	ret

.call0a
    db	F#4, 2
	db  rel, 2
	db	F#4, 4
	db	D#4, 6
	db	F_4, 4
	db	F_4, 4
	db	F_4, 4
	db	D_4, 2
	db	D_4, 4
	db	F#4, 4
	db	F#4, 4
	db	D#4, 6
	db  F_4, 2
	db  rest,2
	ret

.call0b
    db	F#5, 2
	db  rel, 2
	db	F#5, 4
	db	D#5, 6
	db	F_5, 4
	db	F_5, 4
	db	F_5, 4
	db	D_5, 2
	db	D_5, 4
	db	F#5, 4
	db	F#5, 4
	db	D#5, 6
	db  F_5, 2
	db  rest,2
	ret

.call1
	db  E_4, 2, rest, 2
	db  F_4, 2
	db  ChannelVolume, 5, E_4, 2, ChannelVolume, 15
	db  TonePorta, 18, G_4, 4, TonePorta, 0, ChannelVolume, 5, G_4, 2, ChannelVolume, 15
	db  E_4, 1, rest, 3
	db  E_4, 1, rest, 1
	db  C_5, 2
	db  E_4, 1, rest, 1
	db  F_4, 1, rest, 3
	db  G_4, 2
	db  ChannelVolume, 5, E_4, 2, ChannelVolume, 15
	db  A#4, 2, A#4, 2, rest, 2
	db  A#4, 2, ChannelVolume, 5, A#4, 2, ChannelVolume, 15, A#4, 2, rest, 2
	db  A#4, 2, A#4, 2, ChannelVolume, 5, A#4, 2, ChannelVolume, 15
	db  A_4, 2, ChannelVolume, 5, A#4, 2, ChannelVolume, 15
	db  G_4, 2, ChannelVolume, 5, A_4, 2, ChannelVolume, 15
	db  F_4, 2, ChannelVolume, 5, G_4, 2, ChannelVolume, 15
	db  E_4, 2, rest, 2
	db  F_4, 2
	db  ChannelVolume, 5, E_4, 2, ChannelVolume, 15
	db  TonePorta, 18, G_4, 4, TonePorta, 0, ChannelVolume, 5, G_4, 2, ChannelVolume, 15
	db  G_4, 2, rest, 4
	db  C_5, 2
	db  E_5, 1, rest, 1
	db  D_5, 1, rest, 3
	db  C_5, 2, ChannelVolume, 5, E_4, 2, ChannelVolume, 15
	ret

PT_CNZ2_Proto_CH2:
	
	
	;00
	db	SetInstrument,id_Arp2
	db	B_3, 2
	db  rel, 2
	db	B_3, 4
	db	B_3, 6
	db	C#4, 4
	db	C#4, 4
	db	C#4, 4
	db	A#3, 2
	db	A#3, 4
	db	B_3, 4
	db	B_3, 4
	db	B_3, 6
	db  C#4, 2
	db  rest,2
	db	SetInstrument,id_CNZFillLead
	db  E_3, 2, G_3, 2, B_3, 2, E_4, 2, G_4, 2, rest, 2, B_4, 2
	
	;01
	db	SetInstrument,id_Arp2
	db	B_4, 2
	db  rel, 2
	db	B_4, 4
	db	B_4, 6
	db	C#5, 4
	db	C#5, 4
	db	C#5, 4
	db	A#4, 2
	db	A#4, 4
	db	B_4, 4
	db	B_4, 4
	db	B_4, 6
	db  C#5, 2
	db  rest,2
	db	SetInstrument,id_CNZFillLead
	db  E_3, 2, G_3, 2, B_3, 2, D_4, 2, E_4, 2, G_4, 2, B_4, 2
	
	; 02, 03
	;db	SetLoopPoint
	db  SetInstrument, id_Tom,    C_2, 4
	db  ChannelVolume, 8, SetInstrument, id_Arp830, E_4, 4, ChannelVolume, 15
	db  SetInstrument, id_Tom,    C_2, 2
	db  ChannelVolume, 8, SetInstrument, id_Arp950, G_4, 6, ChannelVolume, 15
	db  SetInstrument, id_Tom,    C_2, 4
	db  ChannelVolume, 8, SetInstrument, id_Arp830, E_4, 4, ChannelVolume, 15
	db  SetInstrument, id_Tom,    C_2, 4
	db  ChannelVolume, 8, SetInstrument, id_Arp830, E_4, 4, ChannelVolume, 15
	db  SetInstrument, id_Tom,    C_2, 2
	db  ChannelVolume, 8, SetInstrument, id_Arp950, F_4, 6, ChannelVolume, 15
	db  SetInstrument, id_Tom,    C_2, 4
	db  ChannelVolume, 8, SetInstrument, id_Arp950, F_4, 4, ChannelVolume, 15
	db  SetInstrument, id_Tom,    C_2, 4
	db  ChannelVolume, 8, SetInstrument, id_Arp950, F_4, 4, ChannelVolume, 15
	db  SetInstrument, id_Tom,    C_2, 4
	db  ChannelVolume, 8, SetInstrument, id_Arp950, F_4, 4, ChannelVolume, 15
	
	;db	GotoLoopPoint
	db	EndChannel

PT_CNZ2_Proto_CH3:
    db SetRepeatPoint
	db	SetInstrument, id_Bass4
	db	F#3, 2
	db  rel, 2
	db	F#3, 2, rel, 2
	db	D#3, 6
	db	F_3, 2, rel, 2
	db	F_3, 2, rel, 2
	db	F_3, 2, rel, 4
	db	D_3, 4
	db	F#3, 2, rel, 2
	db	F#3, 2, rel, 2
	db	D#3, 6
	db  F_3, 2
	db  rest,2
	db	SetInstrument, id_TomWave
	db  A_3, 1, A_3, 1, A_3, 2, A_3, 2, F_3, 2, F_3, 2, E_3, 2, E_3, 2
	db	RepeatSection, 2
	
	;db	SetLoopPoint
	db  SetRepeatPoint
	db	SetInstrument, id_Bass3
	db  C_3, 6
	db  G_2, 2
	db  G_2, 6
	db  C_3, 2
	db  C_3, 6
	db  G_2, 2
	db  G_2, 4
	db  C_3, 2
	db  D#3, 2
	
	db  A#2, 6
	db  F_2, 2
	db  F_2, 6
	db  A#2, 2
	db  A#2, 4
	db  F_2, 2
	db  F_2, 2
	db  A#2, 2
	db  D_2, 2
	db  F_2, 2
	db  D_2, 2
	;db	GotoLoopPoint
	db  RepeatSection, 4  ; FIXME: make this section unique
	
	db	SetInstrument, id_WaveLeadMed2
	db  D#6,2, D_6,2, D#6,2, F_6,2, rest,2, D_6,2, rest,2, A#5,10
	db  A#5,4, D#6,4, D_6,1, rest,1, D_6,2, rest,2, D_6,2, rest,2,  A#5,2, rest,2
	db  F_5,6, G_5,4, A#5,4, D_6, 4
	
	db	EndChannel

PT_CNZ2_Proto_CH4:
    dbw CallSection, .call0
    dbw CallSection, .call0
	db	SetLoopPoint
	dbw CallSection, .call1
	dbw CallSection, .call2
	dbw CallSection, .call2
	dbw CallSection, .call2
	db	GotoLoopPoint
	db	EndChannel
.call0
    Drum	CymbL,8
	
	Drum	Snare,2
	Drum    OHH, 2
	Drum	Snare,2
	Drum	Kick, 4
	Drum	Snare, 4
	Drum	Kick, 2
	Drum	Kick, 4
	Drum	Snare, 4
	Drum	CymbL, 8
	Drum	Snare, 2
	Drum	Kick, 4
	Drum	Snare, 2
	db  	fix, 2
	
	Drum	Snare, 1
	Drum	Snare, 1
	Drum	Snare, 2
	Drum	Snare, 2
	Drum	Kick, 2
	Drum	Kick, 2
	Drum	Kick, 2
	Drum	Kick, 2
	ret

.call1
    Drum	CymbL,8
    Drum	Snare, 2
    Drum	CHH, 2
    Drum	OHH, 4
    ret
    
.call2
    Drum	Kick, 2
    Drum	CHH, 2
    Drum	OHH, 4
    Drum	Snare, 2
    Drum	CHH, 2
    Drum	OHH, 4
    ret

PT_CNZ2_Proto_CHNull:
    db  EndChannel
