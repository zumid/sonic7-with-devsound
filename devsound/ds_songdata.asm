; ================================================================
; DevSound song data
; ================================================================

; =================================================================
; Song speed table
; =================================================================

SongSpeedTable:
	db	4,4			; cnz2
	db	4,4			; cnz2
	db	4,4			; cnz2
	db	4,4			; cnz2
	db	4,4			; cnz2
	db	4,4			; cnz2
	db	4,4			; cnz2
	db	4,4			; cnz2
	db	4,4			; cnz2
SongSpeedTable_End


SongPointerTable:
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
	dw	PT_CNZ2_Proto
SongPointerTable_End

if(SongSpeedTable_End-SongSpeedTable) < (SongPointerTable_End-SongPointerTable)
	fail "SongSpeedTable does not have enough entries for SongPointerTable"
endc

if(SongSpeedTable_End-SongSpeedTable) > (SongPointerTable_End-SongPointerTable)
	warn "SongSpeedTable has extra entries"
endc

; == instruments ==
include "devsound/instruments/volumes.asm"
include "devsound/instruments/arps.asm"
include "devsound/instruments/waves.asm"
include "devsound/instruments/vibrato.asm"
include "devsound/instruments/_instruments.asm"

; == songs ==
include "devsound/songs/protocnz.asm"





