rem === RGBDS must be in the Windows path! ===
mkdir obj
rgbasm -h -o obj\patch.o patch.asm
rgblink -n patch.sym -O baserom.gbc -o patchedrom.gb obj\patch.o
rgbfix -p 0xFF -m 0x11 -v patchedrom.gb
pause
