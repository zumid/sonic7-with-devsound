# RGBDS variables
# for if you want to get another version

RGBDSDIR	:= /usr/bin
ASM	:= $(RGBDSDIR)/rgbasm
GFX	:= $(RGBDSDIR)/rgbgfx
LNK	:= $(RGBDSDIR)/rgblink
FIX	:= $(RGBDSDIR)/rgbfix

ROMNAME := patchedrom

#GFXDIR = gfx
#GFXSRC = $(shell find $(GFXDIR) -name *.png)
#GFXOUT = $(patsubst $(GFXDIR)/%.png, $(GFXDIR)/%.2bpp, $(GFXSRC))


all: rom

rom: $(ROMNAME).gb


$(ROMNAME).gb: $(ROMNAME).o
	$(LNK) -n $(ROMNAME).sym -O baserom.gbc -o $(ROMNAME).gb $(ROMNAME).o
	$(FIX) -p 0xFF -m 0x11 -v $(ROMNAME).gb

$(ROMNAME).o: patch.asm $(GFXOUT)
	$(ASM) -h -o $(ROMNAME).o patch.asm

#$(GFXDIR)/%.2bpp: $(GFXDIR)/%.png
#	$(GFX) -o $@ $<


clean:
ifneq ("$(wildcard $(GFXDIR)/*.2bpp)","")
	rm $(GFXDIR)/*.2bpp
endif
ifneq ("$(wildcard *.gb)","")
	rm *.gb
endif
ifneq ("$(wildcard *.sym)","")
	rm *.sym
endif
ifneq ("$(wildcard *.sav)","")
	rm *.sav
endif
ifneq ("$(wildcard *.o)","")
	rm *.o
endif
